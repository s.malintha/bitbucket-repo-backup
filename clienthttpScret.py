#!/usr/bin/python3
#****************************************************************************************************
# Backup all TEAM repositories
# Current Version : v1.00
#
#***************************************************************************************************

from oauthlib.oauth2 import BackendApplicationClient
from requests.auth import HTTPBasicAuth
from requests_oauthlib import OAuth2Session,TokenUpdated
import requests
import json
import os.path
import time
import shutil
import subprocess
import tarfile
import datetime

class clientSecret:
    client_id = #Client ID 
    client_secret = #Clent Secret genarated from bitbucket account 
    token_url = 'https://bitbucket.org/site/oauth2/access_token'
    refresh_url = 'https://bitbucket.org/token'
    protected_url = 'https://bitbucket.org/secret'
    repository_url = 'https://api.bitbucket.org/2.0/repositories/'
    json = ""
    clone_url_list = 'list_of_clone_url'

    def access_token(self):
        try:
            auth = HTTPBasicAuth(self.client_id,self.client_secret)
            client = BackendApplicationClient(client_id=self.client_id)
            oauth = OAuth2Session(client=client)
            token = oauth.fetch_token(token_url=self.token_url, auth=auth)
            access_token = token['access_token']
            return access_token
        except Exception as e:
            print(e)
            exit(1)

    def write_url(self,token,team):
        try:
            url = self.repository_url+team+"?access_token="+token
            #print("URL %s" %url)
            return url
        except Exception as e:
            print(e)
            exit(1)

    def response(self,url):
        try:
            r = requests.get(url)
            self.json = r.json()
            day = datetime.datetime.today().strftime('%Y-%m-%d')
            today = datetime.datetime.strptime(day,"%Y-%m-%d")
            #print(self.json)
            for page in range(len(self.json['values'])):
                with open(self.clone_url_list,'a+') as file_list:
                    update_on = self.json['values'][page]['updated_on'].split('T')[0]
                    updated = datetime.datetime.strptime(update_on,"%Y-%m-%d")
                    diff = today-updated
                    if diff.days <= 180: 
                        file_list.write(self.json['values'][page]['links']['clone'][1]['href'])
                        file_list.write("\n")
                file_list.closed
            if 'next' in self.json:
                self.response(self.json['next'])
            else:
                print("All repos have been written to the %s file"%self.clone_url_list)
        except Exception as e:
             print(e)
             exit(1)
    
    def file_remove(self,file_name):
        try:
            if os.path.exists(self.clone_url_list):
                os.remove(self.clone_url_list)
            else:
                print("File not found")
        except Exception as e:
            print(e)
            exit(1)

    def clone_repos(self):
        try:
            date = str(time.strftime("%Y%m%d"))
            directory = "Repo_Backup_%s"%date
            tarrepo = directory+".tar.gz"
            if os.path.exists(directory):
                shutil.rmtree(directory)
            os.makedirs(directory)
            with open(self.clone_url_list,'r') as clone_list:
                os.chdir(directory)
                #print(os.getcwd())
                for line in clone_list :
                    print(line)
                    cmd = "git clone %s"%line
                    proc = subprocess.Popen([cmd],shell=True ,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
                    (out, stderr) = proc.communicate()
                    if proc.returncode != 0:
                        print(stderr)
                        exit(1)
                    else:
                        print("File cloned successfully!")
            os.chdir('..') 
            print("compressing package...")
            with tarfile.open(tarrepo,"w:gz") as tar:
                tar.add(directory,arcname=os.path.basename(directory))
            print("Package compressed successfully as %s "%tarrepo)
        except Exception as e:
            print(e)
            exit(1)

    def tar_repos(self,output_filename, source_dir):
        try:
            print("compressing package...")
            with tarfile.open(output_filename, "w:gz") as tar:
                tar.add(source_dir, arcname=os.path.basename(source_dir))
            print("Package compressed successfully as %s "%output_filename)
        except Exception as e:
            print(e)
            exit(1)

c = clientSecret()
token = c.access_token()
c.file_remove(c.clone_url_list)
url = c.write_url(token,"TEAM")
print("Writting clone url to the file...")
c.response(url)
c.clone_repos()

